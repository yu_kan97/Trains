package com.example.home.trains;

import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    private final String LINK = "http://mobile.viaggiatreno.it/vt_pax_internet/mobile/numero?lang=EN&numeroTreno=";
    private final int DELAY = 5;

    private TableLayout tableLayout;
    private EditText addTrainText;
    private SharedPreferences prefs;

    private Map<String, String> trains;
    private Map<String, TableRow> trainRows;
    private Map<String, Integer> rowColors;
    private Map<String, Boolean> isTracking;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        trains = new TreeMap<>();
        trainRows = new TreeMap<>();
        rowColors = new TreeMap<>();
        isTracking = new TreeMap<>();

        tableLayout = (TableLayout) findViewById(R.id.MainTableLayout);
        addTrainText = (EditText) findViewById(R.id.AddTrainEditText);

        prefs = PreferenceManager.getDefaultSharedPreferences(this);

        Set<String> set = prefs.getStringSet("Trains", null);
        if (set != null){
            Iterator<String> it = set.iterator();
            while(it.hasNext()){
                String s = it.next();
                trains.put(s, "");
                isTracking.put(s, true);
                rowColors.put(s, Color.rgb(255, 255, 255));//WHITE
                addRow(s);
            }
        }

        setRepeatingAsyncTask();
    }

    private void setRepeatingAsyncTask() {

        final Handler handler = new Handler();
        Timer timer = new Timer();


        TimerTask task = new TimerTask() {

            @Override
            public void run() {
                handler.post(new Runnable() {
                    public void run() {
                        MyTask mt = new MyTask();

                        if (trains.size()==0) return;
                        String[] s = new String[trains.size()];
                        Object[] o = trains.keySet().toArray();
                        for (int i=0; i<s.length; i++){
                            s[i] = (String) o[i];
                        }

                        mt.execute(s);
                        update();
                    }
                });
            }
        };

        timer.schedule(task, 0, DELAY*1000);
    }

    public void onClick(View v){
        if (v.getId()==R.id.AddTrainButton) {
            String number = "";
            if (trains.size() >= 6) {
                Toast.makeText(this, "Can't add more trains", Toast.LENGTH_LONG).show();
                return;
            } else {
                try {
                    number = String.valueOf(addTrainText.getText());
                    if (trains.containsKey(number) || number == null || number.equals("")) {
                        return;
                    }
                    trains.put(number, "");
                    isTracking.put(number, true);
                    rowColors.put(number, Color.rgb(255, 255, 255));//WHITE
                } catch (NumberFormatException e) {
                    e.printStackTrace();
                    Toast.makeText(this, "Wrong number", Toast.LENGTH_LONG).show();
                    return;
                }
            }
            addRow(number);
        } else if (v.getId()%10==0){
            String number = String.valueOf(v.getId()/10);
            if (!trains.containsKey(number)) {
                return;
            }
            String newTrain = String.valueOf(addTrainText.getText());
            if (newTrain == null || newTrain.equals("") || trains.containsKey(newTrain)) return;
            TableRow row = trainRows.get(number);
            ((Button) row.getChildAt(2)).setText("STOP");
            row.getChildAt(1).setId(Integer.parseInt(newTrain+"0"));
            row.getChildAt(2).setId(Integer.parseInt(newTrain+"1"));
            trainRows.put(newTrain, row);
            trainRows.remove(number);
            trains.put(newTrain, "");
            trains.remove(number);
            isTracking.put(newTrain, true);
            isTracking.remove(number);
            rowColors.put(newTrain, Color.rgb(255, 255, 255));//WHITE
            rowColors.remove(number);
            update();
        } else if (v.getId()%10==1){
            String number = String.valueOf((v.getId()-1)/10);
            if (!trains.containsKey(number)) {
                return;
            }
            ((Button) trainRows.get(number).getChildAt(2)).setText("START");
            trainRows.get(number).getChildAt(2).setId(v.getId()+1);
            rowColors.put(number, Color.rgb(192, 192, 192)); //GREY
            isTracking.put(number, false);
            update();
        } else if (v.getId()%10==2){
            String number = String.valueOf((v.getId()-2)/10);
            if (!trains.containsKey(number)) {
                return;
            }
            ((Button) trainRows.get(number).getChildAt(2)).setText("STOP");
            trainRows.get(number).getChildAt(2).setId(v.getId()-1);
            rowColors.put(number, Color.rgb(255, 255, 255)); //WHITE
            isTracking.put(number, true);
            update();
        }

        SharedPreferences.Editor prefs_editor = prefs.edit();
        prefs_editor.putStringSet("Trains", trains.keySet());
        prefs_editor.apply();
        addTrainText.setText("");
    }

    private void addRow(String number){
        TableRow row = new TableRow(this);
        row.setWeightSum(4);
        row.setMinimumHeight(200);

        TableRow.LayoutParams trainLayoutParams = new TableRow.LayoutParams();
        trainLayoutParams.weight = 2;
        trainLayoutParams.width = 0;
        trainLayoutParams.height = TableRow.LayoutParams.MATCH_PARENT;
        TableRow.LayoutParams buttonsLayoutParams = new TableRow.LayoutParams();
        buttonsLayoutParams.weight = 1;
        buttonsLayoutParams.width = 0;
        buttonsLayoutParams.gravity = Gravity.CENTER;

        TextView trainText = new TextView(this);
        trainText.setLines(3);
        trainText.setLayoutParams(trainLayoutParams);
        trainText.setText("Train "+number+"\n"+trains.get(number));
        trainText.setTextSize(25);

        Button editButton = new Button(this);
        editButton.setLayoutParams(buttonsLayoutParams);
        editButton.setTextSize(25);
        editButton.setText("EDIT");
        editButton.setOnClickListener(this);
        int e_id = Integer.parseInt(number+"0");
        editButton.setId(e_id);

        Button stopButton = new Button(this);
        stopButton.setLayoutParams(buttonsLayoutParams);
        stopButton.setTextSize(25);
        stopButton.setText("STOP");
        stopButton.setOnClickListener(this);
        int s_id = Integer.parseInt(number+"1");
        stopButton.setId(s_id);

        row.addView(trainText);
        row.addView(editButton);
        row.addView(stopButton);
        tableLayout.addView(row);
        trainRows.put(number, row);
    }

    private void update(){
        Set<String> keys = trains.keySet();

        Iterator<String> it = keys.iterator();
        while (it.hasNext()){
            try {
                String s = it.next();
                TextView text = (TextView) trainRows.get(s).getChildAt(0);
                if (isTracking.get(s)){
                    text.setText("Train "+s+"\n"+trains.get(s));
                } else {
                    text.setText("Train "+s+" (not tracking)\n"+trains.get(s));
                }
                trainRows.get(s).setBackgroundColor(rowColors.get(s));
            } catch (Exception e) {

            }
        }
    }


    class MyTask extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... params){
            try {
                for (String url : params) {
                    if (isTracking.get(url)) sendGet(url);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
        }

        public void sendGet(String url){
            URL obj = null;
            HttpURLConnection con = null;
            try {
                obj = new URL(LINK+url);
                con = (HttpURLConnection) obj.openConnection();
                con.setRequestMethod("GET");
            } catch (java.io.IOException e) {
                e.printStackTrace();
            }

            //add request header
            con.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36");

            BufferedReader in = null;
            StringBuffer response = null;
            try {
                in = new BufferedReader(
                        new InputStreamReader(con.getInputStream()));
                String inputLine;
                response = new StringBuffer();

                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                in.close();

            } catch (IOException e) {
                e.printStackTrace();
            }

            //print result
            String GET_RESULT = response.toString();

            CharSequence cs = "<!-- SITUAZIONE -->";
            String str = "Il treno";
            String str2 = "<br>";

            if (GET_RESULT.contains(cs)){
                    int index = GET_RESULT.indexOf(str);
                    String text2 = GET_RESULT.substring(index, GET_RESULT.length());
                    int index2 = text2.indexOf(str);
                    text2 = text2.substring(index2, text2.indexOf(str2)).trim().replaceAll("\\s+", " ");

                    Pattern p = Pattern.compile("\\d+");
                    Matcher m = p.matcher(text2);

                try {
                    if (m.find()){
                        int delay = Integer.parseInt(m.group());
                        if (delay >= 10){
                            rowColors.put(url, Color.rgb(255, 0, 0)); //RED
                        } else {
                            rowColors.put(url, Color.rgb(255, 255, 0)); //YELLOW
                        }
                        text2 = "is "+delay+" mins late";
                    } else {
                        if (text2.contains("on time")){
                            rowColors.put(url, Color.rgb(0, 255, 0)); //GREEN
                            text2 = "is on time";
                        } else {
                            rowColors.put(url, Color.rgb(0, 0, 255)); //BLUE
                            text2 = "not traveling";
                        }
                    }
                } catch (NumberFormatException e) {

                }
                trains.put(url, text2);
            }

        }

    }
}
